     ______                        
     | ___ \                       
     | |_/ / ___  _   _ _ __   ___ 
     | ___ \/ _ \| | | | '_ \ / _ \
     | |_/ / (_) | |_| | |_) |  __/
     \____/ \___/ \__, | .__/ \___|
                   __/ | |         
                  |___/|_|         
             
             
A PROGRAMMING LANGUAGE TO LEAP OUT OF THE ORDINARY


Created by Victor (https://github.com/vvv-v)

Wow! Finally GitHub! This is my first repo. I know I'm starting
out kinda hard, but... Creating a language turned out to be really
fun!





Boype is a new a dynamic programming language. It is entirely programmed 
in C++, which is in my opinion a great language, but does not ressemble 
it in any way - it is similar to JavaScript in a way that you can just dump 
the functions you invoke anywhere, and they will run, without the need of a 
`main` or `init` or whatnot.

What's more, its syntax is completely new. It does nearly not use
text keywords at all, but characters (`@` for the loop, `?` for the if).
It is a strange langauge, but very easily readable, much more than C or its 
variants. Moreover, it is simple and quick to write, and its installation is 
very fast.



           Some quick samples

Here are some quick samples of the Boype language!

Comments:


comments.bpe

# This is a comment.

// This is a
multi-line comment. \\




Hello world program:


hello-world.bpe

# invoke the print function
out: "Hello world"



Variables (you will notice that is the same as
invoking functions):


vars.bpe

myvariable: "Hi"
myothervar: tru
blob: fls
blobtwo: 10768
out: blobtwo + myvariable



If then else:


if.bpe

true: tru
true ? (
  out: "hello"
)|(
  out: "bye"
)

# or:

true ?
  out: "hello"
|
  out: "bye"




Loop:


loop.bpe

i: 0 | i < 8 | i : i + 1 @ (
    out: i + 1
)

// Output:
1
2
3
4
5
6
7
8 \\


           Installation and usage

The Clone 'n' Make way!


$ git clone https://github.com/vvv-v/boype Boype
$ cd Boype
$ make


This will create an executable file called "Bpe". To run it, 
use guide below:


$ ./Bpe -h

Use this for help.
The filename for Boype is .bpe .


In order to create an executable file (the ./Bpe thing) in 
your original user's space instead of within the Boype repo, 
simply edit the makefile and add it to your user's space. And 
if you want to change the name of the ./Bpe file, you can 
change the makefile too ;-)
