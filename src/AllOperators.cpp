

#include "../h/AllOperators.h"
#include "../h/ErrorHandler.h"

//shared_ptr<AllOperators> ops(nullptr);
AllOperators* ops=nullptr;

Operator opCreate(string textIn, int leftPrecedenceIn, int rightPrecedenceIn, bool overloadableIn);

void AllOperators::init()
{
	//ops=shared_ptr<AllOperators>(new AllOperators());
	ops=new AllOperators();
}

AllOperators::AllOperators()
{	
	#undef DECLARE_OP
	
	//#define DECLARE_OP(name, text, left, right, overload) putOpInMap(name);
	
	#define DECLARE_OP(name, text, prece, input, overload) putOpInMap(name);
	
	ALL_OPS;
}

void AllOperators::putOpInMap(Operator op)
{
	opsMap[op->getText()]=op;
}

void AllOperators::get(string text, vector<Operator>& out)
{
	int start=0;
	int end=text.size();
	
	while (start<int(text.size()))
	{
		while (true)
		{
			if (end<=start)
			{
				error.log("unknown operator '" + text + "'", SOURCE_ERROR);
			}
			
			auto i=opsMap.find(text.substr(start, end-start));
			
			if (i==opsMap.end())
			{
				end--;
			}
			else
			{
				out.push_back(i->second);
				start=end;
				end=text.size();
				break;
			}
		}
	}
}

bool AllOperators::isOpenBrac(Operator op)
{
	return op==openPeren || op==openSqBrac || op==openCrBrac;
}

bool AllOperators::isCloseBrac(Operator op)
{
	return op==closePeren || op==closeSqBrac || op==closeCrBrac;
}


